[![pipeline status](https://gitlab.cern.ch/apc/susofts/interfaces/plugins/geode-plugin/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/interfaces/plugins/geode-plugin/commits/master)

Geode Plugin
=====================

SurveyPad plugin for interacting with Geode application.

You can find more information about SurveyPad here: <https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad>.

You can find more information about Geode here: <https://confluence.cern.ch/display/SUS/Geode+User+Guide>.

#### Table of Content ####

[Purpose](#purpose)

[Download](#download)

[Documentation](#documentation)
- [User guide](#user-guide)
- [Doxygen](#doxygen)
- [Other](#other)

[Build instructions](#build-instructions)
- [Requirements](#requirements)
- [Generate project](#generate-project)
- [Build](#build)
- [Tests](#tests)

[Contribute](#contribute)
- [Jira](#jira)
- [Pull requests](#pull-requests)
- [Automatic tests](#automatic-tests)

Purpose
-------

this plugin allows interacting with Geode from Survey Pad. The integration of this plugin in SurveyPad allows the user to navigate in Geode as in any other web browser. Moreover, it provides automatic opening of downloaded files for editing in Survey Pad and facilitated upload procedure of the results after they have been calculated.


Download
--------

You can download the last version of the plugin for Windows or Linux from the tag page: https://gitlab.cern.ch/apc/susofts/interfaces/plugins/geode-plugin/-/tags

Documentation
-------------

### User guide ###

See the documentation page: https://confluence.cern.ch/display/SUS/SurveyPad+User+Guide#SurveyPadUserGuide-Geode-plugin

### Doxygen ###

The Doxygen documentation is meant for developers only. Follow the [Build instructions](#build-instructions) to set up your projects. Then you can build the `doc` target to create the Doxygen documentation. You will need [Doxygen](https://www.stack.nl/~dimitri/doxygen/download.html#srcbin) and [GraphViz](http://www.graphviz.org/download/#executable-packages) installed and configured.

Once built, you can open the file `build/html/index.html` as an entry point to the documentation.

### Other ###

You can find further documentation in the folder [doc](./doc). You can also find some [notes on the code](https://confluence.cern.ch/display/SUS/Notes+on+the+code).

Build instructions
------------------

Before starting, you can have a look at the documentation about [Getting started with C++](https://confluence.cern.ch/pages/viewpage.action?pageId=22153013) for the CERN survey applications.

### Requirements ###

This plugin can be built on Windows or Linux. To do so, you need at least:
- a C++17 compiler
- CMake 3.10+
- TUT

For Windows, you can follow the steps in the aforementioned [Getting started with C++](https://confluence.cern.ch/pages/viewpage.action?pageId=22153013) documentation.

For Linux, you have an example of the needed steps in the dockerfiles of the [sus_ci_cppworker](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp) project (the Docker image used to automatically run the tests on GitLab-CI).
Note that the `devtoolset` trick is only necessary on the CC7 (Cern CentOS 7) as it doesn't provide a C++17 compiler by default.

### Generate project ###

We use CMake to generate projects, thus it is possible to generate projects for MSVC, Eclipse, or simple Unix makefiles. See the [CMake Generators documentation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) page.

To generate the project, you need first to create a subdirectory named `build/`, and then run CMake inside:

```bash
# first we download all the submodules
$ git submodule update --init
# then we generate the project
$ mkdir build && cd build/
$ cmake -G "Visual Studio 16 2019" -A x64 ../source # Use another generator here if you wish
```

### Build ###

Once generated, you can open your project in the `build/` subfolder. If you use MSVC, you can open the file `build/GeodePlugin.sln`.

you can see that CMake has generated several targets, among others:
- `ALL_BUILD` builds all except the doxygen documentation
- `ZERO_CHECK` reruns CMake and automatically updates your project
- `doc` builds the Doxygen documentation

### Tests ###

To build the tests, build the target `Tests` and run it. We Use TUT to generate unit tests. Note that the tests are automatically performed on Gitlab-CI for each contribution. You can see the results in the CI page.

Contribute
----------

This is a private CERN repository, thus it doesn't accept contributions from outside CERN.

To report an issue (bug, or feature request), follow the [Jira](#jira) subsection. For development, please read on.

### Jira ###

Any request, bug or development should have a Jira issue. You can create an issue on the [dedicated Jira board](https://its.cern.ch/jira/browse/SUS). This is mandatory for both the users and the developers.

### Pull requests ###

The most up-to-date stable branch is `master`. The beta branch is `appwidevs`. As stable branches, you **must not** commit directly in them. You need to create a specific branch for your on-going development and commit there. As we use CMake, if you add a file, don't forget to add it in one of the `CMakeFile.txt`!

Once you have finished your work, you should create a Pull Request (PR, or Merge Request) from your branch to `appwidevs`. The description of your PR should include a link to the corresponding task in Jira.

Once your PR has been reviewed by another developer and accepted, it can be merged into `appwidevs`. Note that, for the sake of a nice Git history, your branch needs to be up to date with `appwidevs`. If it is not the case, you will have to rebase, either automatically from GitLab if there are no conflicts, or manually otherwise.

### Automatic tests ###

Automatic tests are performed each time you push a commit. These tests include compilation of `ALL_BUILD` target, and running the `Tests` target, all on Linux 64 bits and 64 bits. If the tests don't pass, your PR will not be merged.

For each release, when `master` is updated, GitLab-CI will automatically build the installers.
