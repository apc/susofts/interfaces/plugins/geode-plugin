/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef GEODEPLUGIN_HPP
#define GEODEPLUGIN_HPP

#include <interface/SPluginInterface.hpp>

/**
 * Plugin interface for Geode application.
 *
 * This plugin is meant to be used with SurveyPad and implements the plugin interface for SurveyPad.
 *
 * The objective of Geode-plugin is to be able to present Geode web application in functional web browser. It allows the user to navigate in Geode and it provides
 * automatic opening of downloaded files for editing in SurveyPad and facilitated upload procedure of the results after they have been calculated.
 *
 * ## Entry Point - `plugin_uploadresults`
 * In order to facilitate the upload of result files to Geode the `plugin_uploadresults` entry point was created. Using it, it is possible to place the Geode specific
 * action inside of projects of other plugins. Upon clicking the Geode would be opened and during file selection the calculated result files would be proposed, without
 * the need to navigate to them manually. In principle there should be only one class registered under this entry point name as only Geode provides upload functionality,
 * and this functionality may be accessed by all other plugins.
 *
 * ### Example
 * The classes registered under this name should have two methods: `getUploadAction()` and `getUploadFunction()` - first one returning a `QAction` with previously set
 * title, tips and plugin icon, the other returning the `std::function` taking two strings. When called, the returned function automatically opens Geode window and
 * redirects it to `Calculs/LGC/Insertion coo` website. The first string provided is the filepath to the .coo file to be upload, the second string is the filepath to .lgc
 * file to be uploaded.
 * Below is the example interface of a class that can be registered under `plugin_lexers` entrypoint:
 *
 * @code{cpp}
 * #include <functional>
 *
 * #include <QAction>
 *
 * class EPplugin_uploadresults : public QObject
 * {
 * 	   Q_OBJECT
 *
 * public:
 *     Q_INVOKABLE EPplugin_uploadresults(QObject *parent = nullptr) : QObject(parent) {}
 *
 *     Q_INVOKABLE QAction *getUploadAction(QObject *parent = nullptr);
 *     Q_INVOKABLE std::function<void(const QString &, const QString &)> getUploadFunction();
 * };
 * @endcode
 *
 * ### Entry Point registration
 * In order to register the class one should use the following command:
 *
 * @code{cpp}
 * #include <interface/SPluginLoader.hpp>
 * #include "entrypoints/EPplugin_uploadresults.hpp"
 *
 * SPluginLoader::getPluginLoader().registerEntryPoint("plugin_uploadresults", {&EPplugin_uploadresults::staticMetaObject, owningPluginPointer});
 * @endcode
 */
class GeodePlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	GeodePlugin(QObject *parent = nullptr);
	virtual ~GeodePlugin() override = default;

	virtual const QString &name() const noexcept override { return GeodePlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }

	virtual QString getExtensions() const noexcept override { return QString(); }
	virtual bool isMonoInstance() const noexcept override { return true; }

	// not exposed methods
public:
	static const QString &_name();

	// Url handling
	/** @return part of the Geode address that identifies the `Geode/Calculs/LGC/Création fichier input/Resume` website. */
	static inline const QString _LGC_CREATE_INPUT_RESUME = "614";
	/** @return part of the Geode address that identifies the `Geode/Calculs/LGC/Insertion coo` website. */
	static inline const QString _LGC_UPLOAD_COO_ID = "616";
	/** @return part of the Geode address that identifies the `Geode/Calculs/Chaba` website. */
	static inline const QString _CHABA_CALCULUS_ID = "650";

	/** @return true if the url provided belongs to the Geode domain, false otherwise. */
	static bool _isGeodeDomain(const QUrl &url);
	/** @return Geode homepage url. */
	static const QString &_geodeUrl();
	/** @return string with current ID of the provided Geode url. */
	static QString _getPageID(const QUrl &url);
	/** @return the Geode url with the id of the specific website. */
	static QString _buildGeodeUrl(const QString &pageID);
};

#endif // GEODEPLUGIN_HPP
