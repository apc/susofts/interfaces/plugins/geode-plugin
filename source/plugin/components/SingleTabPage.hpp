/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SINGLETABPAGE_HPP
#define SINGLETABPAGE_HPP

#include <QWebEnginePage>

class QWebEngineView;
class QWebEngineProfile;

/**
 * A subclass of QWebEnginePage page that can be used with QWebEngineView using `QWebEngineView::setPage()` method.
 *
 * It is used by SingleTabView (subclass of QWebEngineView) and its main purpose is to provide specific file selection behavior for insertion of .coo files.
 *
 * It uses @NetworkManager's @QWebEngineProfile to share the cookies with the rest of application.
 * 
 * @see SingleTabView, EPplugin_uploadresults
 */
class SingleTabPage : public QWebEnginePage
{
	Q_OBJECT

public:
	SingleTabPage(QWebEngineProfile *profile, QWebEngineView *parent = nullptr);
	virtual ~SingleTabPage() override;

	/** Sets the provided filepath that may be used for future upload. */
	void setUploadPaths(const QString &cooUploadPath, const QString &lgcUploadPath = "");

protected:
	/**
	 * Preselects the desired file whenever file selection is requested.
	 *
	 * This method changes the behavior of QWebEnginePage::chooseFiles by providing support to mime types which is nonexistent in the qt version.
	 * Moreover, if current website is Geode-related it may provide some additional features. If the proposed file path was not modified, the current dir path from the
	 * settings will be used. Otherwise the previously set path will be used.
	 **/
	QStringList chooseFiles(QWebEnginePage::FileSelectionMode mode, const QStringList &oldFiles, const QStringList &acceptedMimeTypes) override;

private:
	/**
	 * In Geode`s `Calculs/LGC/Insertion coo` website the file selection will be modified with the path that was previously set using `setResultPath`,
	 * provided that the file exists.
	 *
	 * @param oldFiles list containing one QString that is always empty. Can be changed to provide a default path.
	 * @param acceptedMimeType type that is desired by file selection widget.
	 */
	QStringList geodeFilepathModification(const QStringList &oldFiles, const QStringList &acceptedMimeTypes);

private:
	/** pimpl */
	class _SingleTabPage_pimpl;
	std::unique_ptr<_SingleTabPage_pimpl> _pimpl;
};

#endif // SINGLETABPAGE_HPP
