/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SINGLETABVIEW_HPP
#define SINGLETABVIEW_HPP

#include <QWebEngineView>

class QWebEngineProfile;

/**
 * Class handling the view for the webpages, forcing all the websites to open in the same view.
 *
 * Opening in the same view is ensured using `createWindow()` method that uses temporary QWebEngineView to obtain and then set the new url, this solution was based on:
 * https://stackoverflow.com/questions/54920726/how-make-any-link-blank-open-in-same-window-using-qwebengine. As the view supports only one page instance, the context
 * menu should not provide some options of opening the links in new windows.
 *
 * It uses @NetworkManager's @QWebEngineProfile to share the cookies with the rest of application.
 */
class SingleTabView : public QWebEngineView
{
	Q_OBJECT

public:
	SingleTabView(QWebEngineProfile *profile, QWidget *parent = nullptr);
	~SingleTabView() = default;

protected:
	virtual void contextMenuEvent(QContextMenuEvent *event) override;
	virtual QWebEngineView *createWindow(QWebEnginePage::WebWindowType type) override;
};

#endif // SINGLETABVIEW_HPP
