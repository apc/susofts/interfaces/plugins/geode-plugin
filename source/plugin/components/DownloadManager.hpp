/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef DOWNLOADMANAGER_HPP
#define DOWNLOADMANAGER_HPP

#include <QWidget>

class QWebEngineDownloadItem;
class QWebEngineView;

/**
 * Download manager allows to download files by accepting `QWebEngineDownloadItem` and opens these files with corresponding plugin.
 *
 * The download manager works tightly with some QWebEngineView to obtain the information about current website and depending on that it may provide different behaviour.
 * Provided that files are downloaded from Geode-domain different handling of the files may be applicable:
 * - adding the proper extension if the file can be associated with some plugin,
 * - if Chaba active and passive files are downloaded it will allow to select the target directory where files will be downloaded and the corresponding Chaba project will be then created.
 *
 * @see SingleTabView
 */
class DownloadManager : public QWidget
{
	Q_OBJECT

public:
	DownloadManager(QWebEngineView *parent = nullptr);
	virtual ~DownloadManager() override = default;

public slots:
	/** Provides a dialog to select file destination and accepts the download after which it opens the downloaded file. */
	void downloadRequested(QWebEngineDownloadItem *webItem);

private:
	/**
	 * Handle Geode-specific downloads - applicable and used only for items in Geode domain.
	 * It changes or adds fitting extension to the filename if applicable and it may control the download of the next item in case when active and passive files are downloaded for Chaba.
	 *
	 * @param webItem the item can be entirely altered inside the method.
	 */
	void handleGeodeDownload(QWebEngineDownloadItem *webItem);
	/** Selects the download destination, creates the Chaba project file and downloads Chaba active file. */
	void handleChabaActiveDownload(QWebEngineDownloadItem *webItem);
	/** Downloads next file as Chaba passive file. */
	void handleChabaPassiveDownload(QWebEngineDownloadItem *webItem);
	/** When Geode downloads the zip file with multiple LGC projects - create a directory for it, download zip, unpack it and open all lgc files automatically. */
	void handleLGCZipDownload(QWebEngineDownloadItem *webItem);
	/** When Geode asks to open a DFS directory, it downloads the cmd file and read the directory to open from it */
	void handleOpenDirectory(QWebEngineDownloadItem *webItem);

private slots:
	/** Execute javascript to find the variable names in the webpage */
	void getFileNames(bool status);

	/** If extension matches - open file in the system default application - otherwise do nothing */
	bool openExcelFile(const QString &path) const;

private:
	/** pimpl */
	class _DownloadManager_pimpl;
	std::unique_ptr<_DownloadManager_pimpl> _pimpl;
};

#endif // DOWNLOADMANAGER_HPP
