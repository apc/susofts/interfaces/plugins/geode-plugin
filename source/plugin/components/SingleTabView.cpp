#include "SingleTabView.hpp"

#include <memory>

#include <QContextMenuEvent>
#include <QMenu>
#include <QWebEngineProfile>

#include "SingleTabPage.hpp"

SingleTabView::SingleTabView(QWebEngineProfile *profile, QWidget *parent) : QWebEngineView(parent)
{
	setAcceptDrops(false);
	setPage(new SingleTabPage(profile, this));
}

void SingleTabView::contextMenuEvent(QContextMenuEvent *event)
{
	std::unique_ptr<QMenu> menu(page()->createStandardContextMenu());
	auto actions = menu->actions();
	for (auto *action : actions)
	{
		if (action->text() == tr("Open link in new tab") || action->text() == tr("Open link in new window"))
			menu->removeAction(action);
	}
	menu->exec(event->globalPos());
}

QWebEngineView *SingleTabView::createWindow(QWebEnginePage::WebWindowType)
{
	// Create new view
	auto *tempView = new QWebEngineView(this);

	// Set it to pass url to the existing view and delete itself
	connect(tempView, &QWebEngineView::urlChanged, [this, tempView](const QUrl &url) {
		setUrl(url);
		tempView->deleteLater();
	});

	return tempView;
}
