#include "DownloadManager.hpp"

#include <unordered_map>

#include <QDesktopServices>
#include <QFile>
#include <QFileInfo>
#include <QHash>
#include <QMessageBox>
#include <QRegularExpression>
#include <QTemporaryDir>
#include <QTextStream>
#include <QWebEngineDownloadItem>
#include <QWebEngineView>

#include <trinity/GeodeConfig.hpp>
#include <utils/SettingsManager.hpp>
#ifdef Q_OS_WIN
#	include <QDir>
#	include <QProcess>
#else
#	include <QDesktopServices>
#	include <QUrl>
#endif

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <utils/FileDialog.hpp>
#include <utils/ProjectManager.hpp>

#include "GeodePlugin.hpp"

namespace
{
/** Geode domain */
/** Vector of pairs consisting of survey software names and their corresponding file extension */
const std::unordered_map<QString, QString> _EXTENSIONS{{"CALIDIST", ".txt"}, {"CHABA", ".act"}, {"CSGEO", ".txt"}, {"LGC", ".lgc"}, {"RABOT", ".rab"}};
/** Default chaba project file. */
const QString _CHABA_PROJECT = R"(*PREC	5
#projectFileVersion	5
title	Title-Example

tolerance	0.00500

scale	fixed	1.00000
TrX	variable	0.00000
TrY	variable	0.00000
TrZ	variable	0.00000
RotX	variable	0.00000
RotY	variable	0.00000
RotZ	variable	0.00000

activeFile	%1
passiveFile	%2

resultFile	result.out
cooFile	coofile.coo
punchFile	punchfile.pun
)";
} // namespace

class DownloadManager::_DownloadManager_pimpl
{
public:
	/** Single-page view that is used by the Geode plugin */
	QWebEngineView *view = nullptr;
	/** If active file was processed then the passive should be too */
	bool shouldDownloadPassive = false;
	/** Is chaba duo-file download ongoing - in case active file download started but was cancelled this attribute equals false  */
	bool isDownloadOngoing = false;
	/** Path to the location of a Chaba project file. */
	QFileInfo projectPath;

	QString csvFileName;
	QString cdFileName;
};

DownloadManager::DownloadManager(QWebEngineView *parent) : QWidget(parent), _pimpl(std::make_unique<_DownloadManager_pimpl>())
{
	_pimpl->view = parent;
	connect(_pimpl->view->page(), SIGNAL(loadFinished(bool)), this, SLOT(getFileNames(bool)));
}

void DownloadManager::getFileNames(bool status)
{
	if (!GeodePlugin::_isGeodeDomain(_pimpl->view->url()) || !status)
		return;

	QString title = _pimpl->view->page()->title();

	QString javascript = R"""(
		var filename = document.querySelector("#%1");
		filename ? filename.value : "";
	)""";

	_pimpl->view->page()->runJavaScript(javascript.arg("P616_CSV_FILE_NAME"), [this](const QVariant &value) { _pimpl->csvFileName = value.toString(); });
	_pimpl->view->page()->runJavaScript(javascript.arg("P616_CD_FILE_NAME"), [this](const QVariant &value) { _pimpl->cdFileName = value.toString(); });
}

void DownloadManager::downloadRequested(QWebEngineDownloadItem *webItem)
{
	if (!webItem || webItem->state() != QWebEngineDownloadItem::DownloadRequested)
		return;

	// If in Geode
	if (GeodePlugin::_isGeodeDomain(_pimpl->view->url()))
	{
		handleGeodeDownload(webItem);
		// If the download accepted
		if (webItem->state() != QWebEngineDownloadItem::DownloadState::DownloadRequested) // If the state of webitem was modified
			return;
	}

	// Normal download procedure - with possibly modified extension
	QString fileName;
	QString webFileName = webItem->downloadFileName();
	if (webFileName == "cd_file")
		fileName = _pimpl->cdFileName;
	else if (webFileName == "csv_file")
		fileName = _pimpl->csvFileName;
	else
		fileName = webFileName;
	const QString path = getSaveFileName(this, tr("Save as"), fileName);
	if (path.isEmpty())
		return;
	connect(webItem, &QWebEngineDownloadItem::finished, [this, path]() -> void {
		if (SettingsManager::settings().settings<GeodeConfigObject>(GeodePlugin::_name()).autoOpenExcel && openExcelFile(path))
			return; // open handled externally

		ProjectManager::getProjectManager().openFromPath(path);
	});
	QFileInfo p(path);
	webItem->setDownloadDirectory(p.path());
	webItem->setDownloadFileName(p.fileName());
	webItem->accept();
}

void DownloadManager::handleGeodeDownload(QWebEngineDownloadItem *webItem)
{
	// If chaba passive file download ongoing
	if (_pimpl->shouldDownloadPassive)
	{
		handleChabaPassiveDownload(webItem);
		return;
	}

	QFileInfo proposedFilePath(webItem->downloadFileName());
	QString proposedName = proposedFilePath.baseName();

	// If extension is .cmd, we try to open a folder
	if (proposedFilePath.suffix() == "cmd")
	{
		handleOpenDirectory(webItem);
		return;
	}

	// If it is a chaba-specific download - i.e. the computation is done in Geode/Calculus/Chaba
	if (GeodePlugin::_getPageID(_pimpl->view->url()) == GeodePlugin::_CHABA_CALCULUS_ID) // If Geode/Calculus/Chaba, the second element specifies what Geode website is opened
		handleChabaActiveDownload(webItem);

	// If it is a zip file and LGC-specific download - i.e. the computation is done in Geode/Calculus/LGC/Creation/Resume
	if (GeodePlugin::_getPageID(_pimpl->view->url()) == GeodePlugin::_LGC_CREATE_INPUT_RESUME && proposedFilePath.suffix() == "zip")
	{
		handleLGCZipDownload(webItem);
		return;
	}

	// Extension modification
	// If name does not have extension (or has .txt) and has a name corresponding to some plugin (first part before the underscore) - add the relevant extension
	if ((proposedFilePath.suffix().isEmpty() || proposedFilePath.completeSuffix() == "txt"))
	{
		auto it = _EXTENSIONS.find(proposedName.left(proposedName.indexOf('_')));
		if (it != _EXTENSIONS.end())
			webItem->setDownloadFileName(proposedName + it->second);
	}
}

void DownloadManager::handleChabaActiveDownload(QWebEngineDownloadItem *webItem)
{
	_pimpl->shouldDownloadPassive = true;
	const QString path = getSaveFileName(this, tr("Save as"), "downloadedProject.chaba");
	if (path.isEmpty())
	{
		webItem->cancel();
		return;
	}
	_pimpl->projectPath = QFileInfo(path);

	QFile file(_pimpl->projectPath.absoluteFilePath());
	if (!file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Text))
	{
		logCritical() << "A Chaba project file could not be created, download failed!";
		return;
	}
	QTextStream stream(&file);
	QString filename = _pimpl->projectPath.baseName();
	stream << _CHABA_PROJECT.arg(filename + ".act", filename + ".pas");
	file.close();

	webItem->setDownloadDirectory(_pimpl->projectPath.absolutePath());
	webItem->setDownloadFileName(filename + ".act");
	webItem->accept();
	_pimpl->isDownloadOngoing = true;
}

void DownloadManager::handleChabaPassiveDownload(QWebEngineDownloadItem *webItem)
{
	_pimpl->shouldDownloadPassive = false;
	// Check in order not to show the select file window again in case the selection was cancelled before
	if (!_pimpl->isDownloadOngoing)
	{
		webItem->cancel();
		return;
	}
	webItem->setDownloadDirectory(_pimpl->projectPath.absolutePath());
	webItem->setDownloadFileName(_pimpl->projectPath.baseName() + ".pas");
	webItem->accept();
	_pimpl->isDownloadOngoing = false;

	connect(webItem, &QWebEngineDownloadItem::finished, [this]() -> void { ProjectManager::getProjectManager().openFromPath(_pimpl->projectPath.absoluteFilePath()); });
}

void DownloadManager::handleLGCZipDownload(QWebEngineDownloadItem *webItem)
{
	// Get download path
	const QString path = getSaveFileName(this, tr("Save as"), webItem->downloadFileName());
	if (path.isEmpty())
	{
		webItem->cancel();
		return;
	}
	QFileInfo p(path);
	QString destinationPath = QDir::toNativeSeparators(p.path() + QDir::separator() + p.baseName() + QDir::separator());

	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	// Does the directory exist already
	if (QDir(destinationPath).exists())
	{
		msgBox.setText("Cannot download the file - folder with this name already exists");
		msgBox.exec();
		webItem->cancel();
		return;
	}

	// Can folder be created
	if (!QDir().mkdir(destinationPath))
	{
		msgBox.setText("Cannot create the destination folder - please make sure you have write permission for the directory");
		msgBox.exec();
		webItem->cancel();
		return;
	}

	// Download zip to newly created folder
	const QString fileName = p.fileName().endsWith(".zip") ? p.fileName() : p.fileName() + ".zip";
	const QString filePath = QDir::toNativeSeparators(destinationPath + fileName);
	webItem->setDownloadFileName(fileName);
	webItem->setDownloadDirectory(destinationPath);
	webItem->accept();

	// Upon download
	connect(webItem, &QWebEngineDownloadItem::finished, [filePath, destinationPath]() -> void {
	// Unpack zip
#ifdef Q_OS_WIN
		const QString command = QString("& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('%1', '%2'); }").arg(filePath, destinationPath);
		QProcess::execute("powershell.exe", {"-nologo", "-noprofile", "-command", command});
#else
		QProcess::execute("unzip", {filePath});
#endif
		// Open extracted projects
		QDir directory(destinationPath);
		auto lgcFiles = directory.entryList({"*.lgc"}, QDir::Files);
		std::for_each(
			lgcFiles.cbegin(), lgcFiles.cend(), [&directory](const QString &fname) { ProjectManager::getProjectManager().openFromPath(directory.filePath(fname)); });
	});
}

bool DownloadManager::openExcelFile(const QString &filepath) const
{
	// arbitrarily chosen Excel extension
	const QStringList extensions = {
		"xlsx",
		"xlsm",
		"xlsb",
		"xls",
		"xlam",
		"csv",
		"prn",
	};
	if (!extensions.contains(QFileInfo(filepath).suffix()))
		return false;
	if (QDesktopServices::openUrl(QUrl("file:///" + filepath, QUrl::TolerantMode)))
	{
		logInfo() << "GeodePlugin: " + tr("File ") << filepath << tr(" was opened externally.");
		return true;
	}
	return false;
}

void DownloadManager::handleOpenDirectory(QWebEngineDownloadItem *webItem)
{
	QTemporaryDir dir;
	if (!dir.isValid())
	{
		logWarning() << dir.errorString();
		webItem->cancel();
		return;
	}

	webItem->setDownloadDirectory(dir.path());
	webItem->accept();
	connect(webItem, &QWebEngineDownloadItem::finished, [webItem]() -> void {
		QFile file(webItem->downloadDirectory() + '/' + webItem->downloadFileName());
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
			return;

		QTextStream in(&file);
		QString line;
		QRegularExpression regexp(R"""(start explorer "(.+)")""");
		while (!in.atEnd())
		{
			line = in.readLine();
			auto match = regexp.match(line);
			if (match.hasMatch())
			{
#ifdef Q_OS_WIN
				QProcess::startDetached("explorer", {QDir::toNativeSeparators(match.captured(1))});
#else
				QDesktopServices::openUrl(QUrl::fromLocalFile(match.captured(1)));
#endif
				break;
			}
		}
	});
}
