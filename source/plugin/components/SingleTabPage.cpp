#include "SingleTabPage.hpp"

#include <QFileDialog>
#include <QFileInfo>
#include <QWebEngineProfile>
#include <QWebEngineView>

#include <utils/SettingsManager.hpp>

#include "GeodePlugin.hpp"

namespace
{
// Copy of enum coming from internals of FilePickerController class https://code.woboq.org/qt5/qtwebengine/src/core/file_picker_controller.h.html#70
enum FileChooserMode
{
	Open,
	OpenMultiple,
	UploadFolder,
	Save
};

// Function parsing the extensions and creating the filter QString
QString mimelistToString(const QStringList &mimetypes)
{
	QString outFilter;
	for (const QString &mimetype : mimetypes)
		// Mimetype without dot + the paranthesis with properly formatted extension inside
		outFilter.append(mimetype.right(mimetype.length() - 1) + " (*" + mimetype + ");;");
	outFilter.append("All Files (*)");
	return outFilter;
}
} // namespace

class SingleTabPage::_SingleTabPage_pimpl
{
public:
	QFileInfo cooUploadPath;
	QFileInfo lgcUploadPath;
	QWebEngineView *view = nullptr;
};

SingleTabPage::SingleTabPage(QWebEngineProfile *profile, QWebEngineView *parent) : QWebEnginePage(profile, parent), _pimpl(std::make_unique<_SingleTabPage_pimpl>())
{
	_pimpl->view = parent;
}

SingleTabPage::~SingleTabPage() = default;

void SingleTabPage::setUploadPaths(const QString &cooUploadPath, const QString &lgcUploadPath)
{
	_pimpl->cooUploadPath = QFileInfo(cooUploadPath);
	_pimpl->lgcUploadPath = QFileInfo(lgcUploadPath);
}

QStringList SingleTabPage::chooseFiles(QWebEnginePage::FileSelectionMode mode, const QStringList &oldFiles, const QStringList &acceptedMimeTypes)
{
	// Method is heavily based on the default QWebEnginePage::chooseFiles which can be accessed under this link
	// https://code.woboq.org/qt5/qtwebengine/src/webenginewidgets/api/qwebenginepage.cpp.html#2283
	// The main difference is providing custom behavior for file selection for Geode and support for mime types

	QStringList proposedPath = oldFiles;
	// If Geode and result path was set - alter the proposed file path
	if (GeodePlugin::_isGeodeDomain(_pimpl->view->url()) && !_pimpl->cooUploadPath.filePath().isEmpty())
		proposedPath = geodeFilepathModification(oldFiles, acceptedMimeTypes);

	QStringList ret;
	QString str;
	QFileInfo pathInfo;
	if (proposedPath.first().isEmpty())
		pathInfo = SettingsManager::settings().innerSettings("currentDirPath");
	else
		pathInfo = proposedPath.first();

	switch (static_cast<FileChooserMode>(mode))
	{
	case FileChooserMode::OpenMultiple:
		// Go to directory
		ret = QFileDialog::getOpenFileNames(view(), tr("Select multiple files"), pathInfo.absolutePath(), mimelistToString(acceptedMimeTypes));
		break;
	case FileChooserMode::UploadFolder:
		// Go to directory
		str = QFileDialog::getExistingDirectory(view(), tr("Select folder to upload"), pathInfo.absolutePath());
		if (!str.isNull())
			ret << str;
		break;
	case FileChooserMode::Save:
		// Go to directory
		str = QFileDialog::getSaveFileName(view(), tr("Save file as"), pathInfo.absolutePath(), mimelistToString(acceptedMimeTypes));
		if (!str.isNull())
			ret << str;
		break;
	case FileChooserMode::Open:
		// Go to file
		str = QFileDialog::getOpenFileName(view(), tr("Select a file"), pathInfo.absoluteFilePath(), mimelistToString(acceptedMimeTypes));
		if (!str.isNull())
			ret << str;
		break;
	}
	if (!str.isEmpty())
		SettingsManager::settings().innerSettings("currentDirPath", QFileInfo(str).absolutePath());

	return ret;
}

QStringList SingleTabPage::geodeFilepathModification(const QStringList &oldFiles, const QStringList &acceptedMimeTypes)
{
	// If on 'Calculs/LGC/Insertion coo'
	if (GeodePlugin::_getPageID(_pimpl->view->url()) == GeodePlugin::_LGC_UPLOAD_COO_ID)
	{
		// Check which 'File select' widget was clicked (based on its mimeType)
		// If .coo - absolute file path to .coo
		if (!acceptedMimeTypes.isEmpty() && acceptedMimeTypes[0] == ".coo" && !_pimpl->cooUploadPath.filePath().isEmpty())
			return {_pimpl->cooUploadPath.absoluteFilePath()};
		// If .lgc - absolute file path to .lgc
		else if (!acceptedMimeTypes.isEmpty() && acceptedMimeTypes[0] == ".lgc" && !_pimpl->lgcUploadPath.filePath().isEmpty())
			return {_pimpl->lgcUploadPath.absoluteFilePath()};
		// Provide just the directory otherwise
		else
			return {_pimpl->cooUploadPath.absolutePath()};
	}
	return oldFiles;
}
