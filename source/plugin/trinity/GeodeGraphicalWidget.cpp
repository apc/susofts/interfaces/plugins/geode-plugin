#include "GeodeGraphicalWidget.hpp"
#include "ui_GeodeGraphicalWidget.h"

#include <QAction>
#include <QLineEdit>
#include <QShortcut>
#include <QToolBar>
#include <QWebEngineProfile>
#include <QWebEngineView>

#include <editors/web/WebSearch.hpp>
#include <utils/NetworkManager.hpp>

#include "GeodePlugin.hpp"
#include "components/DownloadManager.hpp"
#include "components/SingleTabPage.hpp"
#include "components/SingleTabView.hpp"

class GeodeGraphicalWidget::_GeodeGraphicalWidget_pimpl
{
public:
	std::unique_ptr<Ui::GeodeGraphicalWidget> ui = nullptr;
	DownloadManager *downloadManager = nullptr;
	WebSearch *search = nullptr;
	QWebEngineView *developperTools = nullptr;
	SingleTabView *webView = nullptr;
};

GeodeGraphicalWidget::GeodeGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	SGraphicalScriptWidget(owner, parent), _pimpl(std::make_unique<_GeodeGraphicalWidget_pimpl>())
{
	// Ui
	_pimpl->ui = std::make_unique<Ui::GeodeGraphicalWidget>();
	_pimpl->ui->setupUi(this);
	// WebView
	_pimpl->webView = new SingleTabView(NetworkManager::getNetworkManager().getProfile(), this);
	_pimpl->ui->browsingLayout->addWidget(_pimpl->webView);
	setUrl(GeodePlugin::_geodeUrl());
	// DownloadManager
	_pimpl->downloadManager = new DownloadManager(_pimpl->webView);
	// Search - created dynamically instead of promoting in .ui as it requires SingleTabView parent
	_pimpl->search = new WebSearch(_pimpl->webView);
	_pimpl->ui->browsingLayout->addWidget(_pimpl->search);
	_pimpl->search->hide();
	// Developper tools
	_pimpl->developperTools = new QWebEngineView(this);
	_pimpl->developperTools->setFixedHeight(320);
	_pimpl->developperTools->load(QUrl(URL_DEBUGGING));
	_pimpl->ui->browsingLayout->addWidget(_pimpl->developperTools);
	_pimpl->developperTools->hide();

	setupUi();
	setupScripts();
}

GeodeGraphicalWidget ::~GeodeGraphicalWidget() = default;

void GeodeGraphicalWidget::paste()
{
	SGraphicalWidget::paste();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Paste)->trigger();
}

void GeodeGraphicalWidget::copy() const
{
	SGraphicalWidget::copy();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Copy)->trigger();
}

void GeodeGraphicalWidget::cut() const
{
	SGraphicalWidget::cut();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Cut)->trigger();
}

void GeodeGraphicalWidget::undo()
{
	SGraphicalWidget::undo();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Undo)->trigger();
}

void GeodeGraphicalWidget::redo()
{
	SGraphicalWidget::redo();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Redo)->trigger();
}

void GeodeGraphicalWidget::setUrl(const QString &url)
{
	_pimpl->webView->setUrl(url);
}

void GeodeGraphicalWidget::setUploadPaths(const QString &cooUploadPath, const QString &lgcUploadPath = "")
{
	auto page = qobject_cast<SingleTabPage *>(_pimpl->webView->page());
	if (page)
		page->setUploadPaths(cooUploadPath, lgcUploadPath);
}

void GeodeGraphicalWidget::_newEmpty()
{
	setUrl(GeodePlugin::_geodeUrl());
}

void GeodeGraphicalWidget::setupUi()
{
	// Url editng
	_pimpl->ui->urlEdit->setText(_pimpl->webView->url().toDisplayString());
	connect(_pimpl->ui->urlEdit, &QLineEdit::returnPressed, [this]() -> void { _pimpl->webView->load(QUrl::fromUserInput(_pimpl->ui->urlEdit->text())); });
	connect(_pimpl->webView, &QWebEngineView::urlChanged, [this](const QUrl &url) -> void { _pimpl->ui->urlEdit->setText(url.toDisplayString()); });
	// Search handling
	QObject::connect(new QShortcut(QKeySequence(QObject::tr("Ctrl+F")), _pimpl->webView), &QShortcut::activated, this, &GeodeGraphicalWidget::showSearch);
	// Download handling
	connect(NetworkManager::getNetworkManager().getProfile(), &QWebEngineProfile::downloadRequested, _pimpl->downloadManager, &DownloadManager::downloadRequested);

	// Actions
	auto *toolbar = new QToolBar(owner()->name(), this);
	// Editing actions
	connect(_pimpl->webView, &QWebEngineView::loadStarted, [this]() {
		emit undoAvailable(false);
		emit redoAvailable(false);
	});
	connect(_pimpl->webView, &QWebEngineView::loadFinished, [this]() {
		emit undoAvailable(true);
		emit redoAvailable(true);
	});
	connect(_pimpl->webView, &QWebEngineView::selectionChanged, [this]() {
		auto selectionAvailable = _pimpl->webView->hasSelection();
		emit copyAvailable(selectionAvailable);
		emit cutAvailable(selectionAvailable);
	});
	// Navigation actions
	auto *actPrev = _pimpl->webView->pageAction(QWebEnginePage::WebAction::Back);
	actPrev->setShortcuts(QKeySequence::StandardKey::Back);
	actPrev->setIcon(QIcon(":/actions/go-previous"));
	actPrev->setToolTip(tr("Go back one page"));
	actPrev->setStatusTip(tr("Go back one page"));
	toolbar->addAction(actPrev);
	auto *actNext = _pimpl->webView->pageAction(QWebEnginePage::WebAction::Forward);
	actNext->setShortcuts(QKeySequence::StandardKey::Forward);
	actNext->setIcon(QIcon(":/actions/go-next"));
	actNext->setToolTip(tr("Go forward one page"));
	actNext->setStatusTip(tr("Go forward one page"));
	toolbar->addAction(actNext);
	// Alternating stop/reload action
	auto *actReload = _pimpl->webView->pageAction(QWebEnginePage::WebAction::Reload);
	actReload->setShortcuts(QKeySequence::StandardKey::Refresh);
	actReload->setIcon(QIcon(":/actions/refresh"));
	actReload->setToolTip(tr("Reload current page"));
	actReload->setStatusTip(tr("Reload current page"));
	auto *actStop = _pimpl->webView->pageAction(QWebEnginePage::WebAction::Stop);
	actStop->setShortcuts(QKeySequence::StandardKey::Cancel);
	actStop->setIcon(QIcon(":/actions/close"));
	actStop->setToolTip(tr("Stop page loading"));
	actStop->setStatusTip(tr("Stop page loading"));
	connect(_pimpl->webView, &QWebEngineView::loadStarted, [actReload, actStop]() {
		actReload->setVisible(false);
		actStop->setVisible(true);
	});
	connect(_pimpl->webView, &QWebEngineView::loadFinished, [actReload, actStop]() {
		actReload->setVisible(true);
		actStop->setVisible(false);
	});
	toolbar->addAction(actReload);
	toolbar->addAction(actStop);
	// Home action
	auto *actHome = new QAction(QIcon(":/icons/Geode-icon"), "Geode homepage", toolbar);
	actHome->setToolTip(tr("Go to Geode homepage"));
	actHome->setStatusTip(tr("Go to Geode homepage"));
	connect(actHome, &QAction::triggered, [this]() -> void { setUrl(GeodePlugin::_geodeUrl()); });
	toolbar->addAction(actHome);
	// Debugging action
	auto *actDevelopperTools = new QAction("Developper Tools", toolbar);
	actDevelopperTools->setShortcut(tr("CTRL+SHIFT+I"));
	actDevelopperTools->setToolTip("Open Web Developper Tools");
	actDevelopperTools->setStatusTip("Open Web Developper Tools");
	connect(actDevelopperTools, &QAction::triggered, this, &GeodeGraphicalWidget::showDevelopperTools);
	addAction(Menus::edit, actDevelopperTools);
	// Separator
	toolbar->addSeparator();
	// Add url navbar
	toolbar->addWidget(_pimpl->ui->urlEdit);

	// Set toolbar
	setToolBar(toolbar);
}

void GeodeGraphicalWidget::showDevelopperTools()
{
	_pimpl->developperTools->setVisible(_pimpl->developperTools->isHidden());
	auto page = qobject_cast<SingleTabPage *>(_pimpl->webView->page());
	if (page)
		page->setDevToolsPage(_pimpl->developperTools->page());
}

void GeodeGraphicalWidget::showSearch()
{
	// Showing without firstly hiding the widget will not call showEvent (that is why hide() is called explicitly)
	_pimpl->search->hide();
	_pimpl->search->show();
}
