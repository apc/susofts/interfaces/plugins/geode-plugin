#include "GeodeConfig.hpp"
#include "ui_GeodeConfig.h"

#include <QSettings>

#include "GeodePlugin.hpp"
#include "Version.hpp"

GeodeConfigObject::GeodeConfigObject() : autoOpenExcel(true)
{
}

bool GeodeConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const GeodeConfigObject &oc = static_cast<const GeodeConfigObject &>(o);
	return autoOpenExcel == oc.autoOpenExcel;
}

void GeodeConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(GeodePlugin::_name());
	settings.setValue("autoOpenExcel", autoOpenExcel);
	settings.endGroup();
}

void GeodeConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(GeodePlugin::_name());
	autoOpenExcel = settings.value("autoOpenExcel", true).toBool();
	settings.endGroup();
}

GeodeConfig::GeodeConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::GeodeConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

GeodeConfig::~GeodeConfig() = default;

SConfigObject *GeodeConfig::config() const
{
	auto *config = new GeodeConfigObject();
	config->autoOpenExcel = ui->chkAutoOpenExcel->isChecked();

	return config;
}

void GeodeConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const GeodeConfigObject *>(conf);
	if (config)
	{
		ui->chkAutoOpenExcel->setChecked(config->autoOpenExcel);
	}
}

void GeodeConfig::reset()
{
	GeodeConfigObject config;
	config.read();
	setConfig(&config);
}

void GeodeConfig::restoreDefaults()
{
	GeodeConfigObject config;
	setConfig(&config);
}
