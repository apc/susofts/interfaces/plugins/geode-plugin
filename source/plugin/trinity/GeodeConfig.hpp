/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef GeodeCONFIG_HPP
#define GEODECONFIG_HPP

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class GeodeConfig;
}

struct GeodeConfigObject : public SConfigObject
{
	GeodeConfigObject();
	virtual ~GeodeConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	bool autoOpenExcel;
};

class GeodeConfig : public SConfigWidget
{
	Q_OBJECT

public:
	GeodeConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~GeodeConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	std::unique_ptr<Ui::GeodeConfig> ui;
};

#endif // GEODECONFIG_HPP
