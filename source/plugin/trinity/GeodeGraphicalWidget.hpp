/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef GEODEGRAPHICALWIDGET_HPP
#define GEODEGRAPHICALWIDGET_HPP

#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/script/EditorScriptWidgets.hpp>

/**
 * Graphical interface for Geode Plugin.
 *
 * This graphical interface serves as a web browser for Geode and it provides easier interface to upload results and automatic opening of downloaded files with corresponding plugins.
 *
 * It is restricted to one tab only that displays SingleTabView (QWebEngineView) for presentation of webpages and it provides a basic navigation actions.
 * Besides typical web browser features it provides the ability to:
 * - open downloaded files automatically with plugin related to downloaded data.
 *
 * @see SingleTabView, DownloadManager
 */
class GeodeGraphicalWidget : public SGraphicalScriptWidget
{
	Q_OBJECT

public:
	GeodeGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~GeodeGraphicalWidget() override;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override { return ShareablePointsList(); }
	virtual bool isModified() const noexcept override { return false; }

public slots:
	// SGraphicalWidget
	virtual void paste() override;
	virtual void copy() const override;
	virtual void cut() const override;
	virtual void undo() override;
	virtual void redo() override;

	// Geode-specific
	/** @param url to be set for the view. */
	virtual void setUrl(const QString &url);
	/** Sets upload paths to SingleTabPage. */
	virtual void setUploadPaths(const QString &cooUploadPath, const QString &lgcUploadPath);

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &) override { return true; }
	virtual bool _open(const QString &) override { return true; }
	virtual void _newEmpty() override;

private:
	void setupUi();

private slots:
	void showSearch();
	void showDevelopperTools();

private:
	/** pimpl */
	class _GeodeGraphicalWidget_pimpl;
	std::unique_ptr<_GeodeGraphicalWidget_pimpl> _pimpl;
};

#endif // GEODEGRAPHICALWIDGET_HPP
