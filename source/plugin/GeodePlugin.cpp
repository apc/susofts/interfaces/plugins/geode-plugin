#include "GeodePlugin.hpp"

#include <QUrl>
#include <QUrlQuery>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <interface/SPluginLoader.hpp>

#include "Version.hpp"
#include "entrypoints/EPplugin_uploadresults.hpp"
#include "trinity/GeodeConfig.hpp"
#include "trinity/GeodeGraphicalWidget.hpp"

GeodePlugin::GeodePlugin(QObject *parent) : QObject(parent)
{
}

QIcon GeodePlugin::icon() const
{
	return QIcon(":/icons/Geode-icon");
}

void GeodePlugin::init()
{
	// Register entry points
	SPluginLoader::getPluginLoader().registerEntryPoint("plugin_uploadresults", {&EPplugin_uploadresults::staticMetaObject, this});
	logDebug() << "Plugin '" << name() << "' loaded";
}

void GeodePlugin::terminate()
{
	logDebug() << "Plugin '" << name() << "' unloaded";
}

SConfigWidget *GeodePlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading GeodePlugin::configInterface (GeodeConfig)";
	return new GeodeConfig(this, parent);
}

SGraphicalWidget *GeodePlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading GeodePlugin::graphicalInterface (GeodeGraphicalWidget)";
	return new GeodeGraphicalWidget(this, parent);
}

const QString &GeodePlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

bool GeodePlugin::_isGeodeDomain(const QUrl &url)
{
	// Compares entire link, e.g.: host: apex-sso.cern.ch, path: /pls/htmldb_devdb11/f
	static const QUrl _sgeodeURL(GeodePlugin::_geodeUrl());
	return _sgeodeURL.host() == url.host() && _sgeodeURL.path() == url.path();
}

const QString &GeodePlugin::_geodeUrl()
{
#ifdef NDEBUG
	static const QString _surlGeode = "https://apex-sso.cern.ch/pls/htmldb_accdb/f?p=GEODEAPEX";
#else
	static const QString _surlGeode = "https://apex-sso.cern.ch/pls/htmldb_devdb19/f?p=GEODEAPEXD";
#endif
	return _surlGeode;
}

QString GeodePlugin::_getPageID(const QUrl &url)
{
	if (url.isEmpty() || !GeodePlugin::_isGeodeDomain(url))
		return "";
	QUrlQuery qurl(url);
	auto queryList = qurl.allQueryItemValues("p");
	if (!queryList.isEmpty())
	{
		QStringList splitQuery = queryList.at(0).split(':');
		if (!splitQuery.isEmpty())
			return splitQuery[1];
	}
	return "";
}

QString GeodePlugin::_buildGeodeUrl(const QString &pageID)
{
	return _geodeUrl() + ':' + pageID;
}
