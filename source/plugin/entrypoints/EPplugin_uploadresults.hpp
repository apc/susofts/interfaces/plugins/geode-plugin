/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef EPPLUGIN_UPLOADRESULTS
#define EPPLUGIN_UPLOADRESULTS

#include <functional>

#include <QObject>

class QAction;

/**
 * Entry point class for "plugin_uploadresults" entry point.
 *
 * This class provides all other plugins with the facilitated access to Geode upload .coo file website (Calculs/LGC/Insertion coo).
 * The class exposes the Geode QAction using getUploadAction() method and the redirection to the proper webpage is done using std::function returned from getUploadFunction() method.
 *
 * @see GeodeGraphicalWidget, SingleTabPage
 */
class EPplugin_uploadresults : public QObject
{
	Q_OBJECT

public:
	Q_INVOKABLE EPplugin_uploadresults(QObject *parent = nullptr) : QObject(parent) {}
	Q_INVOKABLE virtual ~EPplugin_uploadresults() override = default;

	/** @return QAction with previously set title, tips and Geode icon. */
	Q_INVOKABLE QAction *getUploadAction(QObject *parent = nullptr) const;
	/**
	 * Returns a function that automatically opens Geode window (and creates it first if it was not created before) and redirects it to `Calculs/LGC/Insertion coo` website.
	 *
	 * @return std::function that navigates to Geode upload url and takes a path to .coo result file and to .lgc file that are meant to be used in file selection in SingleTabPage.
	 */
	Q_INVOKABLE std::function<void(const QString &, const QString &)> getUploadFunction() const;
};

#endif // EPPLUGIN_UPLOADRESULTS
