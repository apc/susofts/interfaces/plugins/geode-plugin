#include "EPplugin_uploadresults.hpp"

#include <memory>

#include <QAction>
#include <QMdiArea>
#include <QMdiSubWindow>

#include <interface/SPluginLoader.hpp>

#include "GeodePlugin.hpp"
#include "trinity/GeodeGraphicalWidget.hpp"
#include "utils/ProjectManager.hpp"

QAction *EPplugin_uploadresults::getUploadAction(QObject *parent) const
{
	auto uploadAction = std::make_unique<QAction>(QIcon(":/icons/Geode-icon"), tr("Upload results"), parent);
	uploadAction->setToolTip(tr("Upload the results to Geode"));
	uploadAction->setStatusTip(tr("Upload the results to Geode"));
	uploadAction->setEnabled(false);

	return uploadAction.release();
}

std::function<void(const QString &, const QString &)> EPplugin_uploadresults::getUploadFunction() const
{
	return [](const QString &cooUploadPath, const QString &lgcUploadPath = "") -> void {
		auto *geodePlg = dynamic_cast<GeodePlugin *>(SPluginLoader::getPluginLoader().getPlugin(GeodePlugin::_name()));
		if (!geodePlg)
			return;

		// Get the window (or create if necessary)
		auto &pm = ProjectManager::getProjectManager();
		auto *window = pm.findMdiWindow(geodePlg);
		if (!window)
		{
			pm.newProject(geodePlg);
			window = pm.findMdiWindow(geodePlg);
		}
		else
			pm.getMdiArea()->setActiveSubWindow(window);
		// Transfer the path to GeodeGraphicalWidget
		auto *gSGW = qobject_cast<GeodeGraphicalWidget *>(window->widget());
		if (gSGW)
		{
			gSGW->setUploadPaths(cooUploadPath, lgcUploadPath);
			gSGW->setUrl(GeodePlugin::_buildGeodeUrl(GeodePlugin::_LGC_UPLOAD_COO_ID));
		}
	};
}
