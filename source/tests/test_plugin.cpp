#include <tut/tut.hpp>

#include <interface/SPluginLoader.hpp>

#include "GeodePlugin.hpp"

// initialization
namespace tut
{
struct test_plugin_data
{
	~test_plugin_data() { SPluginLoader::getPluginLoader().unload(); }
};

typedef test_group<test_plugin_data> tg;
tg test_plugin_group("Plugin tests");
typedef tg::object testobject;
} // namespace tut

// tests
namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("Load plugin");

	auto &loader = SPluginLoader::getPluginLoader();
	ensure_equals(loader.size(), 0);

	loader.load();

	ensure_equals(loader.size(), 1);
	ensure(loader.has(GeodePlugin::_name()));
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("Load plugin from its name");

	auto &loader = SPluginLoader::getPluginLoader();
	ensure_equals(loader.size(), 0);

	loader.load(PROJECT_NAME);

	ensure_equals(loader.size(), 1);
	ensure(loader.has(GeodePlugin::_name()));
}
}; // namespace tut
